package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static final String SQL_ALL_USER_SELECT = "select * from users";
	private static final String SQL_INSERT_USER = "insert into users values (DEFAULT, ?)";
	private static final String SQL_ALL_TEAMS_SELECT = "select * from teams";
	private static final String SQL_INSERT_TEAM = "insert into teams values (DEFAULT,?)";
	private static final String SQL_SELECT_USER_LOGIN = "select * from users where login=?";
	private static final String SQL_SELECT_TEAM_NAME = "select * from teams where name=?";
	private static final String SQL_INSERT_USER_TEAMS = "insert into users_teams values(?,?)";
	private static final String SQL_SELECT_USER_TEAMS = "select UT.team_id as id, TS.name from users_teams UT JOIN teams TS ON UT.team_id=TS.id where user_id=?";
	private static final String SQL_DELETE_USER = "delete from users where login=?";
	private static final String SQL_DELETE_TEAM = "delete from teams where name=?";
	private static final String SQL_UPDATE_TEAM = "update teams set name=? where id=?";
	private static DBManager instance;
	private Connection con;
	private String url;



	private void getURLConnection (){
		try (InputStream input = new FileInputStream("app.properties")) {
			Properties prop = new Properties();
			prop.load(input);
			this.url = prop.getProperty("connection.url");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static synchronized DBManager getInstance() {
		if (instance == null)
		{
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		getURLConnection();
	}

	public List<User> findAllUsers() throws DBException {
		List<User> findAllUsersList = new ArrayList<>();

		try {
			PreparedStatement stmt = getStatementByScript(SQL_ALL_USER_SELECT);
			ResultSet resultSet = stmt.executeQuery();
			while (resultSet.next())
			{
				User user = new User();
				user.setId(resultSet.getInt("id"));
				user.setLogin(resultSet.getString("login"));
				findAllUsersList.add(user);
			}
			con.close();
		} catch (SQLException e)
		{
			throw new DBException("SQL Exception in findAllUsers", e);
		}

		return findAllUsersList;
	}

	public boolean insertUser(User user) throws DBException {
		boolean result;
		try {
			PreparedStatement stmt = getStatementByScript(SQL_INSERT_USER);
			stmt.setString(1, user.getLogin());
			result = stmt.execute();
			con.close();
			user.setId(getUser(user.getLogin()).getId());

		} catch (SQLException e)
		{
			throw new DBException("SQL Exception in insertUser", e);
		}
		return result;

	}

	public boolean deleteUsers(User... users) throws DBException {
		boolean result = false;
		try {
			PreparedStatement stmt = getStatementByScript(SQL_DELETE_USER);
			for (User u: users) {
				stmt.setString(1, u.getLogin());
				result = stmt.execute();
			}
			con.close();
		} catch (SQLException e)
		{
			throw new DBException("SQL Exception in deleteUsers", e);
		}
		return result;
	}

	public User getUser(String login) throws DBException {
		User result = new User();
		try {
			con = DriverManager.getConnection(url);
			PreparedStatement stmt = con.prepareStatement(SQL_SELECT_USER_LOGIN);
			stmt.setString(1,login);
			ResultSet resultSet = stmt.executeQuery();
			resultSet.next();
			result.setId(resultSet.getInt("id"));
			result.setLogin(resultSet.getString("login"));
			con.close();

		} catch (SQLException e)
		{
			throw new DBException("SQL Exception in getUser", e);
		}

		return result;
	}

	public Team getTeam(String name) throws DBException {
		Team result = new Team();
		try {
			PreparedStatement stmt = getStatementByScript(SQL_SELECT_TEAM_NAME);
			stmt.setString(1,name);
			ResultSet resultSet = stmt.executeQuery();
			resultSet.next();
			result.setId(resultSet.getInt("id"));
			result.setName(resultSet.getString("name"));
			con.close();
		} catch (SQLException e)
		{
			throw new DBException("SQL Exception in getTeam", e);
		}
		return result;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> findAllTeamsList = new ArrayList<>();
		try {
			PreparedStatement stmt = getStatementByScript(SQL_ALL_TEAMS_SELECT);
			ResultSet resultSet = stmt.executeQuery();
			while (resultSet.next())
			{
				Team team = new Team();
				team.setId(resultSet.getInt("id"));
				team.setName(resultSet.getString("name"));
				findAllTeamsList.add(team);
			}
			con.close();
		} catch (SQLException e)
		{
			throw new DBException("SQL Exception in findAllTeams", e);
		}
		return findAllTeamsList;
	}

	public boolean insertTeam(Team team) throws DBException {
		boolean result;
		try {
			PreparedStatement stmt = getStatementByScript(SQL_INSERT_TEAM);
			stmt.setString(1, team.getName());
			result = stmt.execute();
			con.close();
			team.setId(getTeam(team.getName()).getId());
		} catch (SQLException e)
		{
			throw new DBException("SQL Exception in insertTeams", e);
		}
		return result;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		boolean result = false;
		User u = getUser(user.getLogin());
		List<Team> teamsFormDB = findAllTeams();
		List<Team> setOfInputTeams = new ArrayList<>();
		for (Team tm: teams) {
			setOfInputTeams.add(teamsFormDB.get(teamsFormDB.indexOf(tm)));
		}
		try {
			PreparedStatement stmt = getStatementByScript(SQL_INSERT_USER_TEAMS);
			con.setAutoCommit(false);
			for (Team t: setOfInputTeams) {
				stmt.setInt(1, u.getId());
				stmt.setInt(2,t.getId());
				result = stmt.execute();
			}
			con.commit();
			con.close();
		} catch (SQLException e) {
			try {
				con.rollback();
				con.close();
			} catch (SQLException ex) {
				throw new DBException("SQL Exception in setTeamsForUser rollback",ex);
			}
			throw new DBException("SQL Exception in setTeamsForUser",e);
		}
		return result;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> findUserTeamsList = new ArrayList<>();
		User u = getUser(user.getLogin());
		try {
			PreparedStatement stmt = getStatementByScript(SQL_SELECT_USER_TEAMS);
			stmt.setInt(1,u.getId());
			ResultSet resultSet = stmt.executeQuery();
			while (resultSet.next())
			{
				Team team = new Team();
				team.setId(resultSet.getInt("id"));
				team.setName(resultSet.getString("name"));
				findUserTeamsList.add(team);
			}
			con.close();
		} catch (SQLException e)
		{
			throw new DBException("SQL Exception in getUserTeams", e);
		}
		return findUserTeamsList;
	}

	public boolean deleteTeam(Team team) throws DBException {
		boolean result;
		try {
			PreparedStatement stmt = getStatementByScript(SQL_DELETE_TEAM);
			stmt.setString(1, team.getName());
			result = stmt.execute();
			con.close();

		} catch (SQLException e)
		{
			throw new DBException("SQL Exception in deleteTeam", e);
		}
		return result;
	}

	public boolean updateTeam(Team team) throws DBException {
		boolean result;
		try {
			PreparedStatement stmt = getStatementByScript(SQL_UPDATE_TEAM);
			stmt.setInt(2, team.getId());
			stmt.setString(1, team.getName());
			result = stmt.execute();
			con.close();
		} catch (SQLException e)
		{
			throw new DBException("SQL Exception in updateTeam", e);
		}
		return result;
	}

	public PreparedStatement getStatementByScript(String script) throws SQLException {
		con = DriverManager.getConnection(url);
		return con.prepareStatement(script);
	}

}
